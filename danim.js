const fs = require('fs');
const Zipper = require('node-zip');
const imageType = require('image-type');
const JIMP = require('jimp');
const http = require('http');
const https = require('https');


function choice(it) {
    return it[Math.floor(it.length * Math.random())];
}

function makeID(size=30, chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-:,') {
    return new Array(size).fill(0).map(() => choice(chars)).join('');
}


if (!fs.existsSync('projects'))
    fs.mkdirSync('projects');

let projects = [];

if (fs.existsSync('projects.json'))
    projects = JSON.parse(fs.readFileSync('projects.json', 'utf-8'));
    
else
    fs.writeFileSync('projects.json', JSON.stringify(projects));


projects._buildPartReference = (refName, partID, options) => {
    return {
        name: refName,
        part: partID,
        rotation: options.rotation || 0,

        offset: {
            x: (options.offset || {}).x || 0,
            y: (options.offset || {}).y || 0
        },

        scale: {
            x: (options.scale || {}).x || 0,
            y: (options.scale || {}).y || 0
        },

        postScale: {
            x: (options.postScale || {}).x || 0,
            y: (options.postScale || {}).y || 0
        },

        brightness: options.brightness != null ? options.brightness : 0,
        alpha: options.alpha != null ? options.alpha : 1
    };
};

projects.allocate = (name) => {
    let id = makeID();
    let res = {
        name: name,
        id: id,
        parts: {},
        anims: []
    };

    fs.mkdirSync(`projects/${id}`);
    fs.mkdirSync(`projects/${id}/parts`);
    fs.mkdirSync(`projects/${id}/anims`);

    projects.push(res);
    projects.updateIndex();
    return projects.$proj(res);
};

projects.updateIndex = () => {
    fs.writeFileSync('projects.json', JSON.stringify(projects));
};

projects.$proj = (data) => {
    if (typeof data === 'number')
        data = projects[data];

    else if (typeof data === 'string') {
        if (!projects.has(data))
            throw new Error(`Warning: No Danim project found with ID ${require('util').inspect(data)}!`);

        else
            data = projects.find(data);
    }

    let wrapper;
    return wrapper = {
        data: data,
        json: JSON.stringify.bind(JSON, data),

        addPart: (name, b64media) => {
            data.parts[name] = {
                content: b64media
            };
            projects.updateIndex();
        },

        addAnim: (name) => {
            let id = makeID(50);
            let adata = {
                id: id,
                name: name,
                frames: []
            };

            data.anims.push(adata);
            projects.updateIndex();
            return wrapper.$anim(adata);
        },

        $anim: (adata) => {
            let anw;
            return anw = {
                data: adata,
                json: JSON.stringify.bind(JSON, adata),
                
                tween: (name, position) => {
                    let f1p = Math.floor(position);
                    let f2p = Math.ceil(position);
                    let alpha = position - f1p;

                    if (position === f1p)
                        return adata.frames[f1p];

                    else if (position === f2p)
                        return adata.frames[f2p];

                    else {
                        let f1 = adata.frames[f1p];
                        let f2 = adata.frames[f2p];

                        let upts = Array.concat(f1.parts, f2.parts);
                        let pns = upts.map((ref) => ref.name);
                        let tweeners = {};

                        let pts = JSON.parse(JSON.stringify(upts.filter((r, i) => {
                            if (pns.indexOf(r) !== i)
                                tweeners[pns.indexOf(r)] = upts[i];

                            return pns.indexOf(r) === i;
                        })));

                        Object.keys(tweeners).forEach((t) => {
                            let nextRef = tweeners[t];
                            let prevRef = upts[t];
                            
                            pts[t].offset.x += (nextRef.offset.x - prevRef.offset.x) * alpha;
                            pts[t].offset.y += (nextRef.offset.y - prevRef.offset.y) * alpha;
                            
                            pts[t].scale.x += (nextRef.scale.x - prevRef.scale.x) * alpha;
                            pts[t].scale.y += (nextRef.scale.y - prevRef.scale.y) * alpha;

                            pts[t].postScale.x += (nextRef.postScale.x - prevRef.postScale.x) * alpha;
                            pts[t].postScale.y += (nextRef.postScale.y - prevRef.postScale.y) * alpha;

                            let phi = (nextRef.rotation - prevRef.rotation + 180);

                            // safe-sign modulo
                            phi = (phi - Math.floor(phi / 360) * 360) - 180;

                            pts[t].rotation += phi * alpha;
                            
                            pts[t].alpha += (nextRef.alpha - prevRef.alpha) * alpha;
                            pts[t].brightness += (nextRef.brightness - prevRef.brightness) * alpha;
                        });

                        let ft = {
                            name: name,
                            offX: 0,
                            offY: 0,
                            width: (f2.width - f1.width * alpha) + f2.width,
                            height: (f2.height - f1.height * alpha) + f2.height,
                            parts: pts
                        };
    
                        adata.frames.insert(f2p, ft);
                        projects.updateIndex();
                        return anw.$frame(ft);
                    }
                },

                addFrame: (name, size={}) => {
                    let f = {
                        name: name,
                        offX: 0,
                        offY: 0,
                        width: size.width || 500,
                        height: size.height || 500,
                        parts: []
                    };

                    adata.frames.push(f);
                    projects.updateIndex();
                    return anw.$frame(f);
                },

                $frame: (f) => {
                    return {
                        data: f,
                        json: JSON.stringify.bind(JSON, f),

                        resize: (width, height) => {
                            f.width = width;
                            f.height = height;
                            projects.updateIndex();
                        },

                        offset: (x, y) => {
                            f.offX += x;
                            f.offY += y;
                            projects.updateIndex();
                        },

                        stitchPart: (stitchName, partID, options) => {
                            f.parts.push(projects._buildPartReference(stitchName, partID, options));
                            projects.updateIndex();
                        }
                    };
                }
            };
        },

        renderFrames: (callback) => {
            return new Promise((resolve) => {
                let maxLen = data.anims.map((a) => a.frames.length).reduce((a, b) => a + b) || 0;
                let l = 0;
                let res = [];

                data.anims.forEach((a) => {
                    for (let i = 0; i < a.frames.length; i++) {
                        let f = a.frames[i];
                        let n = f.name;

                        new JIMP(f.width, f.height, 0x00000000, (dest) => {
                            function _finishUp() {
                                // export rendered frame
                                dest.getBase64('image/png', (b) => {
                                    let rf = {
                                        name: n,
                                        png: new Buffer(b, 'base64')
                                    };
    
                                    res.push(rf);
                                    callback(rf);
                                    
                                    if (++l === maxLen)
                                        resolve(rf);
                                });
                            }
                            
                            let size = f.parts.length;
                            let i = 0;

                            f.parts.forEach((p) => {
                                let part = data.parts[p.part];
                                
                                JIMP.read(part.content).then((pimg) => {
                                    pimg.resize(pimg.width * p.scale.x, pimg.height * p.scale.y);
                                    pimg.rotate(p.rotation * 180 / Math.PI);
                                    pimg.resize(pimg.width * p.postScale.x, pimg.height * p.postScale.y);
                                    pimg.brightness(p.brightness);
                                    
                                    dest.composite(pimg, p.offset.x, p.offset.y, {
                                        mode: JIMP.BLEND_SOURCE_OVER,
                                        opacitySource: 1,
                                        opacityDest: p.alpha
                                    });

                                    if (++i === size) _finishUp();
                                });
                            });
                        });
                    }
                });
            });
        },

        zip: (render = false) => {
            let zip = new Zipper();

            data.parts.forEach((p) => {
                zip.file(`/parts/${p.id}/media.base64`, p.content);
                zip.file(`/parts/${p.id}/name.dat`, p.name);
            });
            
            data.anims.forEach((anim) => {
                zip.file(`/anims/${anim.id}`, JSON.stringify(anim));
            });

            if (render)
                wrapper.renderFrames((f) => {
                    zip.file(`/frames/${f.name}.png`, f.png);
                });

            return zip.generate({ compression: 'DEFLATE' });
        },
    };
};

projects.$proj.loadJson = (j) => {
    return projects.$proj(JSON.parse(j));
};

projects.find = (id) => {
    return projects.filter((p) => p.id === id)[0];
};

projects.has = (id) => {
    return projects.some((p) => p.id === id);
};

projects.imageBase64 = (uri) => {
    let httpApi;

    if (uri.slice(0, 5) === 'https')
        httpApi = https;
        
    else if (uri.slice(0, 4) === 'http')
        httpApi = http;

    else
        throw new Error(`Unsupported protocol: '${uri.split('://')[0]}'!`);

    return new Promise((resolve, reject) => {
        let dat = new Buffer(0);
        
        httpApi.get(uri, (res) => {
            res.on('data', (data) => {
                dat = Buffer.concat([dat, data]);
            });

            res.on('error', reject);
            res.on('end', () => resolve(`data:${imageType(dat).mime};base64,${dat.toString('base64')}`));
        });
    });
};

module.exports = projects;