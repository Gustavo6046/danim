# Welcome to Danim!

This project aims to provide its users with a Node.JS library environment
that empowers them to easily create programs capable of animating 2D parts
together. Included should be a demonstrative web interface, that allows
any user to upload Parts and quickly create animations from within the
browser.

In other words, you give Danim some PNG pieces, and Danim moves and
stitches them together for you. ;)

## To-Do List

    * Write the demonstrative web interface...urgh!
    * Test the tweening.